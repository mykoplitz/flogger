pageData = {};
logs = {};
settings = {'hideSidebar' : 0};
regex = {};
logsToRender = [];
openWindows = 0;

document.addEventListener('keydown', function(a) {
  a.key === 'f' ? hideSidebar() : null;
}, true);

$(document).ready(function() {
  commenceShenanigans();
});

function commenceShenanigans() {
  $('#search-logs-text-field').val('');
  $.ajax({
    url: 'php/get-settings.php',
    type: 'get',
    dataType: 'json',
    success: function(r) {
      settings.path = r[0];
      settings.maxLines = r[1];
      settings.refreshInterval = (r[2] * 1000);
      settings.autoRefresh = r[3];
      settings.scrollLock = r[4];
      settings.consoleLogger = r[5];
      pullLogData();
    }
  });
}

function pullLogData() {
  $.ajax({
    url: 'php/get-everything.php',
    type: 'get',
    dataType: 'json',
    data:{
      path: settings.path,
      maxLines: settings.maxLines
    },
    success: function(r) {
      time = new Date();
      h = time.getHours();
      m = time.getMinutes();
      s = time.getSeconds();
      for (i = 0; i < r.length; i++) {
        tmpObj = {};
        tmpObj.lastUpdated = h + ':' + m + ':' + s;
        l = r[i][0]; // Name of log
        tmpObj.logfile = l;
        logs[l] = r[i][1]; // Size of the file
        tmpObj.logData = r[i][2]; // Log data
        pageData[l] = tmpObj;
        logList = document.getElementById('the-actual-list-of-logs');
        logList.innerHTML += `<li id="selector-${l}" onclick="toggleLogWindow('${l}');"><p>${l}</p></li>`;
      }
    }
  });
  (settings.autoRefresh == 1) ? phoneHome() : null;
}

function openLogWindow(l) {
  if (pageData[l].logData) {
    if (!document.getElementById(`log-block-${l}`) && (openWindows < 8)) {
      openWindows += 1;
      sizeMatters();
      logBlockSelector = document.getElementById(`selector-${l}`);
      logBlockContainer = document.getElementById('log-block-container');
      logBlockSelector.className += " selected-log";
      logBlockContainer.innerHTML += `<div class="log-block" id="log-block-${l}"><div class="log-block-top-bar" id="log-block-top-bar-${l}"><div class="exit-button" id="exit-button-${l}" onclick="closeLogWindow('${l}');"></div><div class="log-block-filename" id="log-block-filename-${l}"><p>${l}</p></div><div class="log-block-last-updated" id="log-block-last-updated-${l}"></div></div><div class="log-block-data" id="log-block-data-${l}"></div></div>`;
      renderLogData(l);
    }
  } else {
    alert('Error reading log');
  }
}

function renderLogData(l, diff) {
  logLastUpdated = document.getElementById(`log-block-last-updated-${l}`);
  logBlockData = document.getElementById(`log-block-data-${l}`);
  logLastUpdated.innerHTML = `Last updated: ${pageData[l].lastUpdated}`;
  logBlockData.innerHTML = '';
  log = pageData[l].logData;
  for (line of log) {
    logBlockData.innerHTML += `${line}<br><br>`;
  }
  (settings.scrollLock == 1) ? slamThatShit() : null;
}

function phoneHome() {
  setInterval(function(){
    time = new Date();
    globalTimestamp = document.getElementById('global-timestamp');
    globalTimestamp.innerHTML = '<p>'+time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds()+'</p>';
    $.ajax({
      url: 'php/get-updates.php',
      type: 'get',
      dataType: 'json',
      data:{
        path: settings.path,
        logs: JSON.stringify(logs)
      },
      success: function(r) {
        (r != 0) ? sliceAndDice(r) : null;
      }
    });
  }, settings.refreshInterval);
}

function sliceAndDice(r) {
  time = new Date();
  h = time.getHours();
  m = time.getMinutes();
  s = time.getSeconds();
  for (i = 0; i < r.length; i++) {
    l = r[i][0]; // Log name
    data = r[i][1]; // Log data
    logs[l] = data[0]; // New filesize
    pageData[l].lastUpdated = h + ':' + m + ':' + s;
    pageData[l].logData = pageData[l].logData.concat(data[1]);
    diff = pageData[l].logData.length - settings.maxLines;
    (diff > 0) ? pageData[l].logData = pageData[l].logData.slice(diff) : null;
    (document.getElementById(`log-block-${l}`) != null) ? logsToRender.push(l) : null;
  }
  renderUpdates();
}

function renderUpdates() {
  for (l of logsToRender) {
    (settings.consoleLogger == 1) ? console.log(`Rendering: ${l} with ${diff} new lines. Trimmed to ${settings.maxLines} lines.`) : null;
    logLastUpdated = document.getElementById(`log-block-last-updated-${l}`);
    logBlockData = document.getElementById(`log-block-data-${l}`);
    logLastUpdated.innerHTML = `Last updated: ${pageData[l].lastUpdated}`;
    logBlockData.innerHTML = '';
    log = pageData[l].logData;
    for (line of log) {
      logBlockData.innerHTML += `${line}<br><br>`;
    }
    (settings.scrollLock == 1) ? slamThatShit() : null;
  }
  logsToRender = [];
}

function slamThatShit() {
  openLogs = document.getElementsByClassName('log-block-data');
  for (i = 0; i < openLogs.length; i++) {
    log = document.getElementById(openLogs[i].id);
    log.scrollTop = log.scrollHeight;
  }
}

function toggleLogWindow(l) {
  (document.getElementById(`log-block-${l}`)) ? closeLogWindow(l) : openLogWindow(l);
}

function sizeMatters() {
  sizeParams = {
    0 : ['calc(100% - 14px)', '100%'],
    1 : ['calc(100% - 14px)', '100%'],
    2 : ['calc(50% - 7px)', '100%'],
    3 : ['calc(50% - 7px)', '50%'],
    4 : ['calc(50% - 7px)', '50%'],
    5 : ['calc(25% - 3.5px)', '50%'],
    6 : ['calc(25% - 3.5px)', '50%'],
    7 : ['calc(25% - 3.5px)', '50%'],
    8 : ['calc(25% - 3.5px)', '50%']
  }
  document.styleSheets[1].cssRules[15].style['width'] = sizeParams[openWindows][0];
  document.styleSheets[1].cssRules[15].style['height'] = sizeParams[openWindows][1];
}

function hideSidebar() {
  if (settings.hideSidebar === 0) {
    settings.hideSidebar = 1;
    columns = '0% 100%';
    display = 'none';
  } else {
    settings.hideSidebar = 0;
    columns = '15% 85%';
    display = 'grid';
  }
  document.styleSheets[1].cssRules[3].style['grid-template-columns'] = columns;
  document.styleSheets[1].cssRules[4].style['display'] = display;
}

function closeLogWindow(l) {
  openWindows -= 1;
  sizeMatters();
  parent = document.getElementById('log-block-container');
  child = document.getElementById(`log-block-${l}`);
  selector = document.getElementById(`selector-${l}`);
  selector.classList.remove('selected-log');
  parent.removeChild(child);
}

function searchLogs() {
  var input, filter, ul, li, a;
  input = document.getElementById('search-logs-text-field');
  ul = document.getElementById('the-actual-list-of-logs');
  li = ul.getElementsByTagName('li');
  filter = input.value.toUpperCase();
  for (i = 0; i < li.length; i++) {
    a = li[i].getElementsByTagName('p')[0];
    (a.innerHTML.toUpperCase().indexOf(filter) > -1) ? li[i].style.display = '' : li[i].style.display = 'none';
  }
}