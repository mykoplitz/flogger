<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	$path = $_GET['path'];
	$maxLines = $_GET['maxLines'];
	$regex = include('../regex');
	$logs = scandir($path);
	$data = array();

  for ($i = 0; $i < sizeof($logs); $i++) {
  	$logName = $logs[$i];
  	$fullPath = $path.$logName;
  	if ((is_file($fullPath)) && (substr(sprintf('%o', fileperms($fullPath)), -1) == 4)) {
  		$log = fopen($fullPath, 'r');
  		$ASCIIencoded = mb_check_encoding(fgets($log), 'ASCII');
  		fclose($log);
	  	if ($ASCIIencoded) {
				$n = 0;
				$logData = array();
				$fileSize = filesize($fullPath);
				$log = popen('tac '.$fullPath, 'r');
				if (array_key_exists($logName, $regex)) {
					while (($line = fgets($log)) && ($n < $maxLines)) {
						if (preg_match($regex[$logName], $line)) {
							array_push($logData, trim(preg_replace('/\s\s+/', ' ', $line)));
							$n++;
						}
					}
				} else {
					while (($line = fgets($log)) && ($n < $maxLines)) {
						array_push($logData, trim(preg_replace('/\s\s+/', ' ', $line)));
						$n++;
					}
				}
				pclose($log);
				array_push($data, array($logName, $fileSize, array_reverse($logData)));
			}
		}
	}
	echo json_encode($data);
?>