<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	$data = array();
	$log = fopen('../config', 'r');

  while (($line = fgets($log)) !== false) {
  	if (($line[0] !== '#') && (trim($line) !== '')) {
			$line = preg_replace('/\n+/', '', trim($line));
	  	array_push($data, $line);
		}
  }
  fclose($log);
  echo json_encode($data);
?>