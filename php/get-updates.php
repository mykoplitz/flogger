<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	$path = $_GET['path'];
	$logs = json_decode($_GET['logs']);
	$regex = include('../regex');
	$data = array();

	foreach ($logs as $logName=>$fileSize) {
		$fullPath = $path.$logName;
		$newFilesize = filesize($fullPath);
  	if ($fileSize < $newFilesize) {
			$logData = array();
			$log = fopen($fullPath, 'r');
			fseek($log, $fileSize);
			if (array_key_exists($logName, $regex)) {
				while ($line = fgets($log)) {
					!preg_match($regex[$logName], $line) ?: array_push($logData, trim(preg_replace('/\s\s+/', ' ', $line)));
				}
			} else {
				while ($line = fgets($log)) {
					array_push($logData, trim(preg_replace('/\s\s+/', ' ', $line)));
				}
			}
			fclose($log);
			array_push($data, array($logName, array($newFilesize, $logData)));
		}
	}
	if (sizeof($data) > 0) {
		echo json_encode($data);
	} else {
		echo 0;
	}
?>