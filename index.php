<html>
	<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/main.js"></script>
		<title>Flogger</title>
  </head>
	<body>
    <div id="bg-image"></div>
    <div id="bg-curtain"></div>
    <div id="main-container">
      <div id="log-list-container">
        <div id="global-timestamp"></div>
        <div id="search-logs">
          <input type="text" id="search-logs-text-field" onkeyup="searchLogs();" placeholder="Search">
        </div>
        <div id="list-of-logs">
          <ul id="the-actual-list-of-logs"></ul>
        </div>
      </div>
      <div id="log-block-container"></div>
    </div>
  </body>
</html>